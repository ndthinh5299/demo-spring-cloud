package com.example.department.controller;

import com.example.department.client.EmployeeClient;
import com.example.department.dto.DepartmentDTO;
import com.example.department.dto.EmployeeDTO;
import java.util.List;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Data
@Log4j2
public class DepartmentController {
  private final EmployeeClient employeeClient;

  @GetMapping
  ResponseEntity<DepartmentDTO> getExampleDepartmentData() {
    DepartmentDTO department = new DepartmentDTO();
    department.setName("D01");
    ResponseEntity<List<EmployeeDTO>> employeeResponse = employeeClient.getExampleEmployeeList();
    department.setEmployeeList(employeeResponse.getBody());

    return ResponseEntity.ok(department);
  }

  @GetMapping("/call-employee-and-getError")
  ResponseEntity callEmployeeAndGetError() {
    ResponseEntity employeeResponse = employeeClient.getError();
    return ResponseEntity.ok(employeeResponse);
  }
}
