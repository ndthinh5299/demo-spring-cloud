package com.example.department.client;

import com.example.department.dto.EmployeeDTO;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("employee-service")
public interface EmployeeClient {
  @RequestMapping(method = RequestMethod.GET, value = "employee/")
  ResponseEntity<List<EmployeeDTO>> getExampleEmployeeList();

  @RequestMapping(method = RequestMethod.GET, value = "employee/test/getError")
  ResponseEntity getError();
}
