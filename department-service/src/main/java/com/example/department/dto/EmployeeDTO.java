package com.example.department.dto;

import lombok.Data;

@Data
public class EmployeeDTO {
  private String code;
  private String name;
  private String departmentName;
}
