package com.example.department.dto;

import java.util.List;
import lombok.Data;

@Data
public class DepartmentDTO {
  private String name;
  List<EmployeeDTO> employeeList;
}
