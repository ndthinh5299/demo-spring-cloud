package com.example.employee.controller;

import com.example.employee.dto.EmployeeDTO;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
  @GetMapping
  ResponseEntity<List<EmployeeDTO>> getExampleEmployeeList() {
    List<EmployeeDTO> employeeList = new ArrayList<>();
    employeeList.add(new EmployeeDTO("code01", "Nguyen Van A", "D01"));
    employeeList.add(new EmployeeDTO("code02", "Nguyen Van B", "D01"));
    employeeList.add(new EmployeeDTO("code03", "Nguyen Van C", "D01"));
    employeeList.add(new EmployeeDTO("code04", "Nguyen Van D", "D01"));

    return ResponseEntity.ok(employeeList);
  }
}
