package com.example.employee.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {
  @GetMapping
  ResponseEntity<String> helloWorld() {
    return ResponseEntity.ok("Employee-service say Hello world!");
  }

  @GetMapping("/getError")
  ResponseEntity<String> getError() throws Exception {
    throw new Exception("Loi");
  }
}
