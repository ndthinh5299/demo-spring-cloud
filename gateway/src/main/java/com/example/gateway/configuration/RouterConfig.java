package com.example.gateway.configuration;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouterConfig {

  @Bean
  public RouteLocator routeLocator(RouteLocatorBuilder builder) {
    return builder.routes()
        .route("department_path_route", predicateSpec -> predicateSpec.path("/department/**")
            .uri("lb://department-service"))

        .route("employee_path_route", predicateSpec -> predicateSpec.path("/employee/**")
            .uri("lb://employee-service"))
        .build();
  }

  @Override
  public boolean equals(Object obj) {
    return super.equals(obj);
  }
}
